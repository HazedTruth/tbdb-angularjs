angular.module("tmdbApp", ["ngRoute", "ui.select2", "ui.bootstrap", "ngSanitize"])
    .config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when("/", {}) // allow this route, but don't need to do anything
            .when("/person/:id", {
                templateUrl: "person-template.html",
                controller: "PersonController"
            })
            .when("/movie/:id", {
                templateUrl: "movie-template.html",
                controller: "MovieController"
            })
            .when("/tv/:id", {
                templateUrl: "tv-template.html",
                controller: "TvController"
            })
            .when("/tv/:id/:credit", {
                templateUrl: "tv-template.html",
                controller: "TvController"
            })
            .otherwise({
                redirectTo: "/"
            });
    }]);

angular.module("tmdbApp")
    .controller("SearchController", ["$scope", "$location", "tmdbApi", function ($scope, $location, tmdbApi) {
        // select2 options, see http://ivaynberg.github.io/select2/
        $scope.selectOptions = {
            allowClear: true,
            formatResult: function (state) {
                return '<span class="badge pull-right">' + state.type + '</span>' + '<img class="searchThumb" src="' + state.img + '" />' + state.text;
            },
            placeholder: "Search",
            query: function (query) {
                var queryData = {results: []};

                tmdbApi.search(query.term)
                    .then(function (data) {
                        for (var ndx = 0; ndx < data.results.length; ndx++) {
                            var item = data.results[ndx];
                            queryData.results.push({
                                id: item.id,
                                text: item.name || item.title,
                                img: tmdbApi.getImage(item.profile_path || item.poster_path, "w45"),
                                type: item.media_type
                            });
                        }
                        query.callback(queryData);
                    }, function (data) {
                        query.callback(queryData);
                    });
            },
            width: "100%"
        };

        // Didn't want to use jQuery, but it was the only way I could find to dynamically set the height
        $("#search").on("select2-opening", function () {
            var height = Math.max($(window).height() - 120, 200);
            $(".select2-results").css("maxHeight", height + "px");
        });

        // navigate and clear search
        $scope.$watch("search", function (value) {
            if (value && value.id) {
                $location.path("/" + value.type + "/" + value.id);
                $scope.search = "";
            }
        });
    }])
    .controller("PersonController", ["$scope", "$routeParams", "tmdbApi", "filterOptions", function ($scope, $routeParams, tmdbApi, filterOptions) {
        $scope.primary = {};
        $scope.list = [];
        $scope.roles = [];
        $scope.filters = filterOptions;
        $scope.isLoading = true;
        tmdbApi.person($routeParams.id)
            .then(function (data) {
                $scope.primary = {
                    name: data.name,
                    img: tmdbApi.getImage(data.profile_path, "w185"),
                    birthday: data.birthday,
                    bio: data.biography
                };
                var creditList = [];
                for (var role in data.combined_credits) {
                    var roleList = data.combined_credits[role];
                    if (roleList.length > 0) {
                        $scope.roles.push(role);
                    }
                    for (var ndx = 0; ndx < roleList.length; ndx++) {
                        var credit = roleList[ndx];
                        if (credit.media_type == "movie") {
                            creditList.push({
                                id: credit.id,
                                name: credit.title,
                                character: credit.character,
                                img: tmdbApi.getImage(credit.poster_path, "w154"),
                                date: credit.release_date,
                                media: credit.media_type,
                                role: role
                            });
                        } else if (credit.media_type == "tv") {
                            creditList.push({
                                id: credit.id,
                                credit: credit.credit_id,
                                name: credit.name,
                                character: credit.character,
                                img: tmdbApi.getImage(credit.poster_path, "w154"),
                                date: credit.first_air_date,
                                episodes: credit.episode_count,
                                media: credit.media_type,
                                role: role
                            });
                        }
                    }
                }
                $scope.list = creditList;
                $scope.isLoading = false;
            }, function (data) {
                $scope.isLoading = false;
            });
    }])
    .controller("MovieController", ["$scope", "$routeParams", "tmdbApi", "filterOptions", function ($scope, $routeParams, tmdbApi, filterOptions) {
        $scope.primary = {};
        $scope.list = [];
        $scope.roles = [];
        $scope.filters = filterOptions;
        $scope.isLoading = true;
        tmdbApi.movie($routeParams.id)
            .then(function (data) {
                $scope.primary = {
                    name: data.title,
                    img: tmdbApi.getImage(data.poster_path, "w185"),
                    date: data.release_date,
                    summary: data.overview,
                    genres: data.genres,
                    status: data.status
                };
                var creditList = [];
                for (var role in data.credits) {
                    var roleList = data.credits[role];
                    if (roleList.length > 0) {
                        $scope.roles.push(role);
                    }
                    for (var ndx = 0; ndx < roleList.length; ndx++) {
                        var credit = roleList[ndx];
                        creditList.push({
                            id: credit.id,
                            name: credit.name,
                            character: credit.character,
                            job: credit.job,
                            img: tmdbApi.getImage(credit.profile_path, "w154"),
                            role: role
                        });
                    }
                }
                $scope.list = creditList;
                $scope.isLoading = false;
            }, function (data) {
                $scope.isLoading = false;
            });
    }])
    .controller("TvController", ["$scope", "$routeParams", "tmdbApi", "filterOptions", function ($scope, $routeParams, tmdbApi, filterOptions) {
        $scope.primary = {};
        $scope.list = [];
        $scope.roles = [];
        $scope.filters = filterOptions;
        $scope.isLoading = true;
        $scope.hasCredit = $routeParams.credit;
        $scope.isCreditLoading = true;
        $scope.credit = {
            episodes: [],
            seasons: []
        };
        tmdbApi.tv($routeParams.id)
            .then(function (data) {
                $scope.primary = {
                    id: $routeParams.id,
                    name: data.name,
                    img: tmdbApi.getImage(data.poster_path, "w185"),
                    date: data.first_air_date,
                    summary: data.overview,
                    episodes: data.number_of_episodes,
                    seasons: data.number_of_seasons,
                    genres: data.genres,
                    status: data.status
                };
                var creditList = [];
                for (var role in data.credits) {
                    var roleList = data.credits[role];
                    if (roleList.length > 0) {
                        $scope.roles.push(role);
                    }
                    for (var ndx = 0; ndx < roleList.length; ndx++) {
                        var credit = roleList[ndx];
                        creditList.push({
                            id: credit.id,
                            credit: credit.credit_id,
                            name: credit.name,
                            character: credit.character,
                            job: credit.job,
                            img: tmdbApi.getImage(credit.profile_path, "w154"),
                            role: role
                        });
                    }
                }
                $scope.list = creditList;
                $scope.isLoading = false;
            }, function (data) {
                $scope.isLoading = false;
            });
        tmdbApi.credit($routeParams.credit)
            .then(function (data) {
                var ndx;
                $scope.credit.person = {
                    name: data.person.name,
                    id: data.person.id
                };
                for (ndx = 0; ndx < data.media.episodes.length; ndx++) {
                    var episode = data.media.episodes[ndx];
                    $scope.credit.episodes.push({
                        episode: episode.episode_number,
                        season: episode.season_number,
                        name: episode.name,
                        summary: episode.overview,
                        img: tmdbApi.getImage(episode.still_path, "w154"),
                        date: episode.air_date
                    })
                }
                for (ndx = 0; ndx < data.media.seasons.length; ndx++) {
                    var season = data.media.seasons[ndx];
                    $scope.credit.seasons.push({
                        season: season.season_number,
                        img: tmdbApi.getImage(season.poster_path, "w154"),
                        date: season.air_date
                    })
                }
                $scope.isCreditLoading = false;
            }, function (data) {
                $scope.isCreditLoading = false;
            });
    }])
    .factory("tmdbApi", ["$q", "$http", function ($q, $http) {
        var apiKey = "7ca599c59014b5d3217b07fe2a321e6f";
        var getUrl = "http://api.themoviedb.org/3";

        function getApi(path, params) {
            params.api_key = apiKey;
            var paramList = [];
            for (var key in params) {
                paramList.push(encodeURI(key) + "=" + encodeURI(params[key]));
            }
            var url = getUrl + path + "?" + paramList.join("&");
            var deferred = $q.defer();
            $http.get(url)
                .success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        return {
            search: function (s) {
                return getApi("/search/multi", {
                    query: s,
                    include_adult: false,
                    search_type: "ngram"
                });
            },
            person: function (id) {
                return getApi("/person/" + id, {
                    append_to_response: "combined_credits"
                });
            },
            movie: function (id) {
                return getApi("/movie/" + id, {
                    append_to_response: "credits"
                });
            },
            tv: function (id) {
                return getApi("/tv/" + id, {
                    append_to_response: "credits"
                });
            },
            credit: function (id) {
                return getApi("/credit/" + id, {});
            },
            getImage: function (profilePath, size) {
                size = size || "original";
                if (!profilePath) {
                    // note, need to add new images as we use more sizes
                    return "filler/" + size + ".png";
                }
                return "http://image.tmdb.org/t/p/" + size + profilePath;
            }
        };
    }])
    .filter("radio", function () {
        return function (list, attr, value) {
            attr = attr || '';
            value = value || '';
            return _.filter(list, function (item) {
                return value == "all" || item[attr] == value;
            });
        };
    })
    .value("filterOptions", {
        role: "all",
        media: "all",
        view: "list"
    });